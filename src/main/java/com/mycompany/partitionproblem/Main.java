/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.partitionproblem;

import static com.mycompany.partitionproblem.Partition.findPartition;
import static com.mycompany.partitionproblem.Partition.isSubsetSum;

/**
 *
 * @author kitti
 */
public class Main {

    public static void main(String[] args) {
        int arr[] = {1, 3, 3, 2, 3, 2};
        int arrLen = arr.length;

        System.out.println(isSubsetSum(arr, arrLen, 0));
        if (findPartition(arr, arrLen)) {
            System.out.println("Can divide");

        } else {
            System.out.println("Can not divide");
        }
    }

}
